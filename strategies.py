from constants import BOTTOM
from copy import copy

def retourner_graph(graphe):
    graphe_retourne = {}
    for k in graphe:
        graphe_retourne[k] = []

    for i in graphe:
        for k in graphe[i]:
            if not i in graphe_retourne[k]:
                graphe_retourne[k].append(i)
            
    return graphe_retourne


def creationW(jeu, t,SansBottom=True):

    W = [[{t}]]
    graphe_jeu,Vb,Vr = jeu
    graphe_ret = retourner_graph(graphe_jeu)
    j = 0

    while W[j][-1] != set(graphe_ret.keys()):

        W[j].append(copy(W[j][-1]))
        for p in Vb:
            for q in Vb[p]:
                if q in W[j][-2]: W[j][-1].add(p)
        for p in Vr:
            test = True
            for q in Vr[p]:
                if q not in W[j][-2] and q != BOTTOM:  test = False
            if test: W[-1][-1].add(p)


        derniers = (W[j][0], W[j][1])

        while derniers[0] != derniers[1]:
            W[j].append(copy(W[j][-1]))
            for p in Vb:
                for q in Vb[p]:
                    if q in W[j][-2]: W[j][-1].add(p)
            for p in Vr:
                test = True
                for q in Vr[p]:
                    if q not in W[j][-2] and q != BOTTOM: test = False
                if test: W[-1][-1].add(p)
             
            derniers = (derniers[1], W[j][-1])

        del W[j][-1]

        if (W[-1][-1] | set((BOTTOM,))) == set(graphe_ret.keys()):
            break

        j += 1
        W.append([])
        W[-1].append(copy(W[-2][-1]))

        for p in Vr:
            if p not in W[-2][-1]:
                for q in Vr[p]:
                    for r in Vr[p]:
                        if q in W[-2][-1] and r not in W[-2][-1]: W[-1][0].add(p)

    return W    
