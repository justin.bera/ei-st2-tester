from constants import BOTTOM, TAU, TEMP


def jeuAssocie(automate, SansBottom=True):

    # Jeu est un tuple (V,Vb,Vr) de dictionnaires dont les clés sont les noeuds et les valeurs les ensembles atteignables depuis ces noeuds
    V = {BOTTOM: {}}
    Vb = {BOTTOM: {}}
    Vr = {}

    for noeud in automate:
        V[noeud] = set(automate[noeud].values())
        # Le noeud est rouge ssi toutes les transitions partant du noeud commencent par "!"
        estRouge = True
        for transitionVersVoisin in automate[noeud]:
            if transitionVersVoisin[0] != "!" and transitionVersVoisin != TAU:
                estRouge = False
        if estRouge:
            Vr[noeud] = set(automate[noeud].values())
            Vr[noeud].add(BOTTOM)
            V[noeud].add(BOTTOM)
        else:
            Vb[noeud] = set(automate[noeud].values())

    # On traite le cas où deux noeuds de la même couleur sont voisins.
    for noeud in automate:
        for noeudVoisin in V[noeud]:
            if noeud in Vb and noeudVoisin in Vb:
                nouveauNoeud = (TEMP, noeud, noeudVoisin)
                # Ce nouveau noeud remplace noeudVoisin en tant que voisin de noeud (dans V comme dans Vb)
                Vb[noeud].remove(noeudVoisin)
                Vb[noeud].add(nouveauNoeud)
                V[noeud].remove(noeudVoisin)
                V[noeud].add(nouveauNoeud)
                # Ce nouveau noeud est rouge et possède pour voisins {noeudVoisin, BOTTOM} (dans V comme dans Vr)
                Vr[nouveauNoeud] = set((noeudVoisin,BOTTOM))
                V[nouveauNoeud] = set((noeudVoisin,BOTTOM))
            elif noeud in Vr and noeudVoisin in Vr:
                nouveauNoeud = (TEMP, noeud, noeudVoisin)
                Vr[noeud].remove(noeudVoisin)
                Vr[noeud].add(nouveauNoeud)
                V[noeud].remove(noeudVoisin)
                V[noeud].add(nouveauNoeud)
                Vb[nouveauNoeud] = set((noeudVoisin,))
                V[nouveauNoeud] = set((noeudVoisin,))

        if SansBottom:
            for V_ in (V,Vb,Vr):
                V_.pop(BOTTOM, None)
                for noeud in V_:
                    V_[noeud].discard(BOTTOM)
                    


    return V, Vb, Vr
