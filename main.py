from subprocess import Popen, PIPE

from game import jeuAssocie
from initial import graph, graphTP
from monPrograme import distributeur, distributeur_defectueux, distributeur_en_rupture_de_soda, automateS, automateTP
from strategies import creationW
from tester import testAutom
from test import test, TestSysteme
from SUT import tourne_pas_a_pas

def f2(q):
    return tourne_pas_a_pas(q["neighbors"], q["nodes"], [], [])

print("Notre automate initial censé représenter modèle de notre système :", automateS)
print("\n\nLe TP associé, arbre sous graphe de l'automate :", automateTP)

tp0 = "g1"
s0 = "g0"
jeu = jeuAssocie(automateS)
Vb = jeu[1]
Vr = jeu[2]
graphe = jeu[0]
grapheTP = jeuAssocie(automateTP)[0]

print("\n\nNotre représentation sous forme de jeu:")
print("Graphe:", graphe)
print("Vb:", Vb)
print("Vr:", Vr)
print("GrapheTP:",grapheTP)


verdict = TestSysteme(distributeur, automateS, s0, automateTP, tp0, compterCoups=True, limiteCompteur = 50, automateACouvrir=True, dt=0)
print("Verdict:", verdict)
