class Constant:
    
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

BOTTOM = Constant("bottom")
TAU = Constant("tau")
PREAMBLE = Constant("preamble")
TESTBODY = Constant("testbody")
POSTAMBLE = Constant("postamble")
END = Constant("end")
TEMP = Constant("temp")
