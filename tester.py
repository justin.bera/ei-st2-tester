from constants import BOTTOM, TESTBODY, POSTAMBLE, END, PREAMBLE, TEMP, Constant

def testAutom(graph, graphTP, Vb, Vr, W_tp, W_s, tp0, s0):
    G = {}
    for k in graph:
        G[(k, PREAMBLE)] = set()
        G[(k, TESTBODY)] = set()
        G[(k, POSTAMBLE)] = set()
        G[(k, END)] = set()
    print("\n\tCONSTRUCTION DU TESTER : ")
    print("\n\tAjout des arrêtes sortantes des NOEUDS BLEUS...\n")
    for n in Vb:

        for v in Vb[n]:
            #On traite ici les états de mode préambule. Tous les états sauf tp0 sont succeptibles d'être dans le mode préambule.
            if n!=tp0:
                if v==tp0:
                    G[(n,PREAMBLE)].add((v, TESTBODY))
                else:
                    for j in range(len(W_s)):
                        for i in range(1,len(W_s[j])):
                            if n in W_s[j][i] and v in W_s[j][i-1]:
                                G[(n, PREAMBLE)].add((v, PREAMBLE))
            
            #On traite ici les états en mode postambule.
            if n!=s0:
                if v!=s0:
                    for j in range(len(W_s)):
                        for i in range(1,len(W_s[j])):
                            if n in W_s[j][i] and v in W_s[j][i-1]:
                                G[(n, POSTAMBLE)].add((v, POSTAMBLE))
                elif s0 != tp0:
                    G[(n, POSTAMBLE)].add((s0, PREAMBLE))
                else:
                    G[(n, POSTAMBLE)].add((tp0, TESTBODY))

            #On traite ici les états en mode Test Body.
            if n in graphTP and len(graphTP[n])!=0:
                for v in Vb[n]:
                    if v in graphTP:
                        if len(graphTP[v])==0:
                            G[(n, TESTBODY)].add((v, POSTAMBLE))
                        else:
                            G[(n, TESTBODY)].add((v, TESTBODY))
                    else:
                        if v!=tp0:
                            G[(n, TESTBODY)].add((v, PREAMBLE))
                        else:
                            G[(n, TESTBODY)].add((v, TESTBODY))



    print("\n\tAjout des arrêtes sortantes des NOEUDS ROUGES...\n")
    for n in Vr:

        for v in Vr[n]:

            if n!=tp0:
                if v==tp0:
                    G[(n,PREAMBLE)].add((v, TESTBODY))
                else:
                    G[(n,PREAMBLE)].add((v, PREAMBLE))
            
            if n != s0:
                if v != s0:
                    G[(n,POSTAMBLE)].add((v, POSTAMBLE))
                elif s0 != tp0:
                    G[(n,POSTAMBLE)].add((s0, PREAMBLE))
                else:
                    G[(n,POSTAMBLE)].add((tp0, TESTBODY))
            
            if n in graphTP and len(graphTP[n])!=0:
                if v not in graphTP:
                    G[(n,TESTBODY)].add((v, PREAMBLE))
                elif len(graphTP[v])!=0:
                    G[(n,TESTBODY)].add((v, TESTBODY))
                elif v==tp0:
                    G[(n,TESTBODY)].add((v, TESTBODY))
                elif v==s0:
                    G[(n,TESTBODY)].add((v, PREAMBLE))                    
                else:
                    G[(n,TESTBODY)].add((v, POSTAMBLE))


    """
    for q in G:
        for r in G[q]:
            if r[0] is tuple:  # on test si r est un temporaire
                print(r[0][0])

                if r[0][0] == TEMP:
                    for l in G[r]:
                        G[q].add(l)
                    G[q].pop(r)

    
    val = True
    while val:
        for q in G:
            if isinstance(q[0], tuple):  # on test si r est un temporaire
                print(q[0])
                if q[0][0] == TEMP:
                    G[q] = None
                    continue
        val = False
    """
    return G
