from constants import BOTTOM, TESTBODY, POSTAMBLE, END, PREAMBLE
from game import jeuAssocie
from strategies import creationW
from tester import testAutom
from random import *
from time import sleep

def TestSysteme(systeme, automateS, s0, automateTP, tp0, compterCoups=True, limiteCompteur=float("inf"), dt=0, automateACouvrir=None):
    if automateACouvrir:
        automateACouvrir = automateTP
    jeu = jeuAssocie(automateS)
    grapheJeuS, Vb, Vr = jeu
    grapheJeuTP        = jeuAssocie(automateTP)[0]
    W_tp = creationW(jeu, tp0)
    W_s = creationW(jeu, s0)
    tester = testAutom(grapheJeuS, grapheJeuTP, Vb, Vr, W_tp, W_s, tp0, s0)
    return test(tester, tp0, s0, systeme, compterCoups=compterCoups, limiteCompteur=limiteCompteur, dt=dt, automateACouvrir=automateACouvrir)

def test(tester, tp0, s0, f, compterCoups=True, limiteCompteur=float("inf"), dt=0, automateACouvrir=None):
    print("Debut test")
    egaliteInitiale = (tp0 == s0)
    notPremierTour = False
    q = (s0, PREAMBLE)
    if egaliteInitiale:
        q = (s0, TESTBODY)
    
    compteur = 0
    verdict = True

    if automateACouvrir!=None:
        n_noeuds = len(automateACouvrir)
        liste_noeuds = list(automateACouvrir.keys())

    while q[1] != END and verdict and not(egaliteInitiale and q[0] == s0 and notPremierTour):
        notPremierTour = True
        etat_bleu, mode = q
        print("\nEtat bleu:",q)
        sleep(dt)
        
        #On choisit un état rouge auquel on va accéder
        etat_rouge, mode_rouge = choice(list(tester[q]))
        print("Testeur bleu choisi l'état rouge", (etat_rouge, mode_rouge))
        print("Etat rouge:", (etat_rouge, mode_rouge))
        sleep(dt)
        nouvel_etat_bleu = f(etat_rouge)
        if None != nouvel_etat_bleu:
            print("Programme rouge choisi",nouvel_etat_bleu)
        else:
            nouvel_etat_bleu = etat_rouge[2]
            print("Programme rouge choisi",nouvel_etat_bleu,"par défaut")

        if automateACouvrir != None:
            try:
                liste_noeuds.remove(nouvel_etat_bleu)
            except: pass
            try:
                liste_noeuds.remove(etat_rouge)
            except: pass
            print("Noeuds du TP exploré à {}%".format(100-100*len(liste_noeuds)/n_noeuds))

        q2 = (nouvel_etat_bleu, None)

        if not nouvel_etat_bleu in [etat for etat, mode in tester[(etat_rouge, mode_rouge)]]:
            print("<!> Inconformité : le noeud {} n'est pas censé suivre {}".format(nouvel_etat_bleu, etat_rouge))
            verdict = False
            break

        # if nouvel_etat == BOTTOM:
        #     print("On est tombé dans un noeud bottom")
        #     verdict = False
        #     continue 

        for k in [TESTBODY, POSTAMBLE, END, PREAMBLE]:  ##?????
            if (nouvel_etat_bleu, k) in tester[(etat_rouge, mode_rouge)]:
                q2 = (nouvel_etat_bleu, k)
                break
        
        if not q2[1]:
            print("Erreur: Coincé")
            verdict = False
            continue
        
        if compterCoups: 
            compteur += 2
            print("Coups joués: "+str(compteur))
            if compteur > limiteCompteur:
                break
        

        q = q2
    
    return verdict
