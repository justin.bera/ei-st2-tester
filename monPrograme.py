from random import *

def distributeur(g):
    #A chaque état g parmi g1 (analyse de la pièce) ou g4 (service ou non d'un soda), la machine associe un état suivant 
    if g=="g1" or g=="g4":
        noeud = choice(["g0","g2"])
        return noeud

def distributeur_defectueux(g):
    if g=="g1":
        noeud = choice(["g0","g2"])
        return noeud
    elif g=="g4":
        noeud = choice(["g0","g2","g1"])
        return noeud

def distributeur_en_rupture_de_soda(g):
    if g=="g1":
        noeud = choice(["g0","g2"])
        return noeud
    if g=="g4":
        noeud = "g2"
        return noeud


automateS = {    "g0":{"?Coin":"g1"},
                "g1":{"!Good":"g2", "!Bad":"g0"},
                "g2":{"?Cancel":"g0", "?Soda": "g4"},
                "g4":{"!Can":"g0", "!Unav":"g2"}
                }

automateTP = {
                "g1":{"!Good":"g2"}, "g2":{"?Cancel":"g0", "?Soda": "g4"},
                "g4":{"!Can":"g0"},
                "g0":{}
                }

# TP = {
#                 "g1":{"!Good":"g2"}, "g2":{"?Soda": "g4"},
#                 "g4":{},
#                 }


















